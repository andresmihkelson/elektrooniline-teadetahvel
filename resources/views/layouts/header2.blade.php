<div class="row">
    <div class="col-sm-2"><img id="logo" src="https://assets.ope.ee/logos/nrg/100/2x" alt=""></div>
    <div class="col-sm-offset-1 col-sm-9">
        <ul class="nav nav-pills">
            <li role="presentation"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">Create</a></li>
            <li role="presentation"><a href="#">Login</a></li>
        </ul>
    </div>
</div>